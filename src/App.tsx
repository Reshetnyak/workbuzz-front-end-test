import React from 'react'
import { CssBaseline, ThemeProvider } from '@mui/material'

import { theme } from './theme/theme'
import Header from './components/Header/Header'
import ComicsPage from './Pages/Comics/Comics'

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header />
      <ComicsPage />
    </ThemeProvider>
  )
}

export default App
