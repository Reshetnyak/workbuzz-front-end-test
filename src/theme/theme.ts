import { createTheme } from '@mui/material'
import { colors } from './colors'
import { fonts, fontWeights } from './fonts'

declare module '@mui/material/styles' {
  interface Palette {
    primary20: colors.primary20
    white: colors.white
    neutral10: colors.neutral10
    neutral30: colors.neutral30
    neutral70: colors.neutral70
  }

  interface PaletteOptions {
    primary20?: colors.primary20
    white?: colors.white
    neutral10?: colors.neutral10
    neutral30?: colors.neutral30
    neutral70?: colors.neutral70
  }

  export default function createPalette (palette: PaletteOptions): Palette

  interface PaletteColor {
    darker?: string
  }

  interface SimplePaletteColorOptions {
    darker?: string
  }
}

export const theme = createTheme({
  palette: {
    background: {
      default: colors.background
    },
    primary: {
      main: colors.primary
    },
    primary20: colors.primary20,
    white: colors.white,
    neutral10: colors.neutral10,
    neutral30: colors.neutral30,
    neutral70: colors.neutral70
  },
  components: {
    MuiContainer: {
      styleOverrides: {
        root: {
          color: colors.white
        }
      }
    },
    MuiTypography: {
      styleOverrides: {
        root: {
          color: colors.white
        }
      }
    }
  },
  typography: {
    fontFamily: fonts.default,
    fontSize: 16,
    h1: {
      fontFamily: fonts.condensed,
      fontWeight: fontWeights[fonts.condensed],
      fontSize: '1.801875rem',
      '@media (min-width:1200px)': {
        fontSize: '2.75rem'
      }
    },
    h2: {
      fontFamily: fonts.condensed,
      fontWeight: fontWeights[fonts.condensed],
      fontSize: '1.601875rem',
      '@media (min-width:1200px)': {
        fontSize: '2.3125rem'
      }
    },
    h3: {
      fontFamily: fonts.condensed,
      fontWeight: fontWeights[fonts.condensed],
      fontSize: '1.42375rem',
      '@media (min-width:1200px)': {
        fontSize: '1.9375rem'
      }
    },
    h4: {
      fontFamily: fonts.condensed,
      fontWeight: fontWeights[fonts.condensed],
      fontSize: '1.265625rem',
      '@media (min-width:1200px)': {
        fontSize: '1.5625rem'
      }
    },
    h5: {
      fontFamily: fonts.condensed,
      fontWeight: fontWeights[fonts.condensed],
      fontSize: '1.125rem',
      '@media (min-width:1200px)': {
        fontSize: '1.3125rem'
      }
    },
    h6: {
      fontFamily: fonts.condensed,
      fontWeight: fontWeights[fonts.condensed],
      fontSize: '1rem',
      '@media (min-width:1200px)': {
        fontSize: '1.125rem'
      }
    },
    subtitle1: {
      fontFamily: fonts.condensed,
      fontWeight: fontWeights[fonts.condensed],
      fontSize: '1rem',
      '@media (min-width:1200px)': {
        fontSize: '1.125rem'
      }
    },
    subtitle2: {
      fontFamily: fonts.condensed,
      fontWeight: fontWeights[fonts.condensed],
      fontSize: '0.88875rem',
      '@media (min-width:1200px)': {
        fontSize: '0.875rem'
      }
    },
    body1: {
      fontSize: '1rem',
      '@media (min-width:1200px)': {
        fontSize: '1.125rem'
      }
    },
    body2: {
      fontSize: '0.88875rem',
      '@media (min-width:1200px)': {
        fontSize: '0.875rem'
      }
    }
  }
})
