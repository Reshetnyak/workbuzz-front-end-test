export enum fonts {
  default = 'Roboto, sans-serif',
  condensed = 'Roboto Condensed, sans-serif'
}

export const fontWeights = {
  [fonts.default]: 400,
  [fonts.condensed]: 700
}

export const lineHeights = {
  [fonts.default]: 1.4
}
