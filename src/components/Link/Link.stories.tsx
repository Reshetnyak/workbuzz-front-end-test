import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'
import LinkComponent from './Link'
import { Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, useTheme } from '@mui/material'

export default {
  title: 'Components/Link',
  component: LinkComponent,
  argTypes: {
    label: {
      type: 'string',
      description: 'Link text'
    },
    onClick: {
      type: 'function',
      description: 'Click handler'
    },
    href: {
      type: 'string',
      description: 'Anchor tag `href` attribute',
      defaultValue: 'javascript:void(0)'
    },
    target: {
      type: 'string',
      description: 'Anchor tag `target` attribute',
      defaultValue: '_blank'
    },
    id: {
      type: 'string',
      description: 'Anchor tag `id` attribute'
    },
    children: {
      type: 'function',
      description: 'React Element/s'
    }
  },
  parameters: {
    controls: { expanded: true }
  }
} as ComponentMeta<typeof LinkComponent>

const Template: ComponentStory<typeof LinkComponent> = (args) => <Box sx={{ display: 'flex' }}>
  <LinkComponent {...args}/>
</Box>

export const Link = Template.bind({})

Link.args = {
  label: 'Link',
  href: 'https://workbuzz.com/'
}

export const WithChildren = Template.bind({})

WithChildren.args = {
  onClick: e => { e.preventDefault(); console.log('clicked') },
  children: <Typography variant={'body1'}>Reading list (0)</Typography>
}

const rows = [
  {
    state: 'Default',
    component: <LinkComponent label="Link"/>
  },
  {
    state: 'Hover',
    component: <LinkComponent label="Link" id="hover"/>
  },
  {
    state: 'Focus',
    component: <LinkComponent label="Link" id="focus"/>
  },
  {
    state: 'Active',
    component: <LinkComponent label="Link" id="active"/>
  }
]

export const All: ComponentStory<typeof LinkComponent> = () => {
  const theme = useTheme()
  return <TableContainer component={Paper} sx={{ maxWidth: 350 }}>
        <Table sx={{ backgroundColor: theme.palette.background.default }}>
          <TableHead>
            <TableRow>
              <TableCell align="left"><Typography>State</Typography></TableCell>
              <TableCell align="left"><Typography>Component</Typography></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.state}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  <Typography>{row.state}</Typography>
                </TableCell>
                <TableCell align="left">
                    <div style={{ display: 'flex' }}>{row.component}</div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
}

All.parameters = {
  pseudo: {
    hover: '#hover',
    focus: '#focus',
    active: '#active'
  }
}
