import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Link from './Link'

it('should render Link component', () => {
  const label = 'Link'
  const { getByText } = render(<Link label={label} onClick={() => {}}/>)
  expect(getByText(label)).toBeInTheDocument()
})
