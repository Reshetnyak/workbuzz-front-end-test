import React from 'react'
import type { LinkProps } from './Link.props'
import link from '../../assets/link.svg'
import { Icon, StyledLink } from './Link.s'
import { Typography } from '@mui/material'

const Link: React.FC<LinkProps> = ({
  id,
  label,
  href = 'javascript:void(0)',
  target = '_blank',
  onClick,
  children
}) => (
  <StyledLink id={id} href={href} target={target} onClick={onClick}>
    <Icon src={link} alt="link"/>
    {typeof children === 'undefined'
      ? <Typography component={'span'} variant={'body1'}>{label}</Typography>
      : children
    }
  </StyledLink>
)

export default Link
