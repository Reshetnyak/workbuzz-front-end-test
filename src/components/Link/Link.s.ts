import { styled } from '@mui/material'

export const Icon = styled('img')(({ theme }) => ({
  marginRight: '0.5rem',
  [theme.breakpoints.down('lg')]: {
    height: 20
  },
  [theme.breakpoints.up('lg')]: {
    height: 24
  }
}))

export const StyledLink = styled('a')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  flex: '0 1 auto',
  textDecoration: 'none',
  border: '2px solid transparent',
  ':hover': {
    borderBottom: `2px solid ${theme.palette.neutral70}`
  },
  ':focus': {
    outline: `2px solid ${theme.palette.white}`
  },
  ':active': {
    outline: `2px solid ${theme.palette.neutral70}`
  }
}))
