import type { MouseEventHandler, ReactNode } from 'react'

export interface LinkProps {
  label?: string
  onClick?: MouseEventHandler
  href?: string
  target?: '_blank' | '_self' | '_parent' | '_top'
  id?: string
  children?: ReactNode
}
