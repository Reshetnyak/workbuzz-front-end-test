import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Navigation from './Navigation'

it('should render Navigation component', () => {
  const { getByText } = render(<Navigation total={3} onListOpen={() => {}} />)
  expect(getByText('Comics')).toBeInTheDocument()
  expect(getByText('(3)')).toBeInTheDocument()
})
