import type { MouseEventHandler } from 'react'

export interface NavigationProps {
  total: number
  onListOpen: MouseEventHandler
  title?: string
}
