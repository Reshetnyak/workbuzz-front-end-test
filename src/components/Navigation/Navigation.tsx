import React from 'react'
import { Typography } from '@mui/material'

import { NavigationContainer } from './Navigation.s'
import type { NavigationProps } from './Navigation.props'
import ReadingListLink from '../ReadiltListLink/ReadingListLink'

const Navigation: React.FC<NavigationProps> = ({ total, onListOpen, title = 'Comics' }) => (
  <NavigationContainer>
    <Typography variant='h1'>{title}</Typography>
    <ReadingListLink total={total} onClick={onListOpen} />
  </NavigationContainer>
)

export default Navigation
