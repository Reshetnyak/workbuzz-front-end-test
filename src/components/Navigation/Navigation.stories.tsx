import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'

import NavigationComponent from './Navigation'

export default {
  title: 'Components/Navigation',
  component: NavigationComponent,
  argTypes: {
    total: {
      type: 'number',
      description: 'Number of items added to the Reading List',
      required: true
    },
    onListOpen: {
      type: 'function',
      description: 'Reading List Link click handler',
      required: true
    },
    title: {
      type: 'string',
      description: 'Page title',
      defaultValue: 'Comics'
    }
  },
  parameters: {
    controls: { expanded: true }
  }
} as ComponentMeta<typeof NavigationComponent>

const Template: ComponentStory<typeof NavigationComponent> = (args) => <NavigationComponent {...args}/>

export const Navigation = Template.bind({})

Navigation.args = {
  total: 11,
  onListOpen: e => {
    e.preventDefault()
    console.log('Reading list link clicked')
  }
}
