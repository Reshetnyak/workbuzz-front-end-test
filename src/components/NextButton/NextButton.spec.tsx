import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import NextButton from './NextButton'

it('should render Next Button component', () => {
  const { getByText } = render(<NextButton onClick={() => {}}/>)
  expect(getByText('Next')).toBeInTheDocument()
})
