import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'
import NextButton from './NextButton'

export default {
  title: 'Buttons/Next',
  component: NextButton,
  argTypes: {
    onClick: {
      type: 'function',
      description: 'Click handler',
      required: true
    },
    label: {
      type: 'string',
      description: 'Button text',
      defaultValue: 'Next'
    },
    id: {
      type: 'string',
      description: 'Button `id` attribute'
    },
    disabled: {
      type: 'boolean',
      description: 'If `true` the button is disabled',
      default: false
    }
  },
  parameters: {
    controls: { expanded: true }
  }
} as ComponentMeta<typeof NextButton>

const Template: ComponentStory<typeof NextButton> = (args) => <NextButton {...args}/>

export const Next = Template.bind({})

Next.args = {
  label: 'Next',
  disabled: false
}
