import React from 'react'
import type { NextButtonProps } from './NextButton.props'
import rightArrow from '../../assets/rightArrow.svg'
import Button from '../Button/Button'
import { Icon } from './NextButton.s'

const NextButton: React.FC<NextButtonProps> = ({ id, label = 'Next', disabled = false, onClick }) => {
  return <Button
    id={id}
    label={label}
    primary={false}
    rightIcon={<Icon src={rightArrow} alt="right arrow"/>}
    disabled={disabled}
    onClick={onClick}
  />
}

export default NextButton
