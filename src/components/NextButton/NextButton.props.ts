import type { MouseEventHandler } from 'react'

export interface NextButtonProps {
  onClick: MouseEventHandler
  id?: string
  label?: string
  disabled?: boolean
}
