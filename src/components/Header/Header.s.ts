
import { Box, styled } from '@mui/material'

export const HeaderContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderBottom: `1px solid ${theme.palette.neutral30}`,
  padding: '0.5rem 0 0.25rem 0'
}))
