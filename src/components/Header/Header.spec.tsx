import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Header from './Header'

it('hould render Header component', () => {
  render(<Header />)
  const displayedImage = document.querySelector('img') as HTMLImageElement
  expect(displayedImage.src).toContain('logo')
})
