import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'

import HeaderComponent from './Header'

export default {
  title: 'Components/Header',
  component: HeaderComponent
} as ComponentMeta<typeof HeaderComponent>

const Template: ComponentStory<typeof HeaderComponent> = (args) => <HeaderComponent {...args}/>

export const Header = Template.bind({})
