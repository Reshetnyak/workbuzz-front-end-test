import React from 'react'
import logo from '../../assets/logo.svg'

import { HeaderContainer } from './Header.s'

const Header: React.FC = () => {
  return (
    <HeaderContainer>
      <img src={logo} />
    </HeaderContainer>
  )
}

export default Header
