import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'

import FilterComponent from './Filter'

export default {
  title: 'Components/Filter',
  component: FilterComponent,
  argTypes: {
    items: {
      control: {
        type: 'array'
      },
      description: 'Filter items',
      required: true
    },
    label: {
      type: 'string',
      defaultValue: 'Filter by character:',
      description: 'Filter label'
    },
    onFilter: {
      type: 'function',
      description: 'Item click handler',
      required: true
    }
  },
  args: {
    items: [
      { id: 1, label: '3-D Man' },
      { id: 2, label: 'A-Bomb (HAS)' },
      { id: 3, label: 'A.I.M' },
      { id: 4, label: 'Aaron Stack' },
      { id: 5, label: 'Abomination (Emil Blonsky)' },
      { id: 6, label: 'Amazing Grisha' }
    ],
    onFilter: item => { console.log(item) }
  },
  parameters: {
    controls: { expanded: true }
  }
} as ComponentMeta<typeof FilterComponent>

const Template: ComponentStory<typeof FilterComponent> = (args) => <FilterComponent {...args}/>

export const Filter = Template.bind({})

export const LoadingState = Template.bind({})

LoadingState.args = {
  items: [],
  onFilter: item => { console.log(item) }
}
