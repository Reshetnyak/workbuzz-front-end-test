import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Filter from './Filter'

it('should render Filter component', () => {
  const items = [
    { id: 1, label: 'First' },
    { id: 2, label: 'Second' },
    { id: 3, label: 'Third' }
  ]
  const { getByText } = render(<Filter
    items={items}
    onFilter={id => { console.log(id) }}
  />)
  expect(getByText('First')).toBeInTheDocument()
  expect(getByText('Second')).toBeInTheDocument()
  expect(getByText('Third')).toBeInTheDocument()
})
