import { Box, styled, Typography } from '@mui/material'

export const FilterContainer = styled(Box)(({ theme }) => ({
  borderRadius: '16px',
  backgroundColor: theme.palette.neutral30,
  [theme.breakpoints.down('lg')]: {
    padding: '1rem'
  },
  [theme.breakpoints.up('lg')]: {
    padding: '1.5rem'
  }
}))

export const Label = styled(Typography)(({ theme }) => ({
  color: theme.palette.neutral70
}))

export const ItemsContainer = styled('div')(({ theme }) => ({
  display: 'flex',
  flexWrap: 'wrap',
  [theme.breakpoints.down('lg')]: {
    gap: '0.5rem 1.5rem'
  },
  [theme.breakpoints.up('lg')]: {
    gap: '1rem 2rem'
  }
}))
