export interface FilterItem {
  id: number
  label: string
}

export interface FilterProps {
  items: FilterItem[]
  onFilter: (id: number) => void
  label?: string
}
