import React from 'react'
import { Skeleton, Typography } from '@mui/material'

import type { FilterProps } from './Filter.props'
import { FilterContainer, Label, ItemsContainer } from './Filter.s'
import { colors } from '../../theme/colors'

const Filter: React.FC<FilterProps> = ({
  items,
  onFilter,
  label = 'Filter by character:'
}) => {
  return (
    <FilterContainer>
      <Label variant={'subtitle1'}>{label}</Label>
      <ItemsContainer>
      {
        items.length === 0
          ? [150, 200, 110, 220, 70].map(width => <Skeleton
              key={width}
              variant="text"
              width={width}
              sx={{ fontSize: '1rem', backgroundColor: colors.neutral70 }}
            />)
          : items.map(item => <Typography
              key={item.id}
              variant={'body1'}
              onClick={() => { onFilter(item.id) }}
              sx={{ cursor: 'pointer' }}
            >{item.label}</Typography>)
      }
      </ItemsContainer>
    </FilterContainer>
  )
}

export default Filter
