import React from 'react'
import plus from '../../assets/plus.svg'
import type { AddButtonProps } from './AddButton.props'

import Button from '../Button/Button'

const AddButton: React.FC<AddButtonProps> = ({ id, label = 'Add', disabled = false, onClick }) =>
  <Button
    id={id}
    label={label}
    primary
    leftIcon={<img src={plus} alt="plus" width={14}/>}
    disabled={disabled}
    onClick={onClick}
  />

export default AddButton
