import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'

import AddButton from './AddButton'

export default {
  title: 'Buttons/Add',
  component: AddButton,
  argTypes: {
    onClick: {
      type: 'function',
      description: 'Click handler',
      required: true
    },
    id: {
      type: 'string',
      description: 'Button `id` attribute'
    },
    label: {
      type: 'string',
      description: 'Button text'
    },
    disabled: {
      type: 'boolean',
      description: 'If `true` the button is disabled',
      defaultValue: false
    }
  },
} as ComponentMeta<typeof AddButton>

const Template: ComponentStory<typeof AddButton> = (args) => <AddButton {...args}/>

export const Add = Template.bind({})

Add.args = {
  label: 'Add',
  disabled: false
}
