import type { MouseEventHandler } from 'react'

export interface AddButtonProps {
  onClick: MouseEventHandler
  id?: string
  label?: string
  disabled?: boolean
}
