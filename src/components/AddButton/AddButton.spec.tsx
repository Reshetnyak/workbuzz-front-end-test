import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import AddButton from './AddButton'

it('should render Add Button component', () => {
  const { getByText } = render(<AddButton onClick={() => {}}/>)
  expect(getByText('Add')).toBeInTheDocument()
})
