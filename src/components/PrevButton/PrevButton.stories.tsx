import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'

import PrevButton from './PrevButton'

export default {
  title: 'Buttons/Prev',
  component: PrevButton,
  argTypes: {
    onClick: {
      type: 'function',
      description: 'Click handler',
      required: true
    },
    label: {
      type: 'string',
      description: 'Button text',
      defaultValue: 'Prev'
    },
    id: {
      type: 'string',
      description: 'Button `id` attribute'
    },
    disabled: {
      type: 'boolean',
      description: 'If `true` the button is disabled',
      default: false
    }
  },
  parameters: {
    controls: { expanded: true }
  }
} as ComponentMeta<typeof PrevButton>

const Template: ComponentStory<typeof PrevButton> = (args) => <PrevButton {...args}/>

export const Prev = Template.bind({})

Prev.args = {
  label: 'Prev',
  disabled: false
}
