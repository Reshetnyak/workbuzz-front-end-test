import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import PrevButton from './PrevButton'

it('should render Prev Button component', () => {
  const { getByText } = render(<PrevButton onClick={() => {}}/>)
  expect(getByText('Prev')).toBeInTheDocument()
})
