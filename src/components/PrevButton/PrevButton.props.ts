import type { MouseEventHandler } from 'react'

export interface PrevButtonProps {
  onClick: MouseEventHandler
  id?: string
  label?: string
  disabled?: boolean
}
