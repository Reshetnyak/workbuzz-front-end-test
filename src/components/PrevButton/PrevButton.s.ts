import { styled } from '@mui/material'

export const Icon = styled('img')(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    width: 15
  },
  [theme.breakpoints.up('lg')]: {
    width: 17
  }
}))
