import React from 'react'
import leftArrow from '../../assets/leftArrow.svg'
import type { PrevButtonProps } from './PrevButton.props'
import Button from '../Button/Button'
import { Icon } from './PrevButton.s'

const PrevButton: React.FC<PrevButtonProps> = ({ id, label = 'Prev', disabled = false, onClick }) =>
  <Button
    id={id}
    label={label}
    primary={false}
    leftIcon={<Icon src={leftArrow} alt="left arrow"/>}
    disabled={disabled}
    onClick={onClick}
  />

export default PrevButton
