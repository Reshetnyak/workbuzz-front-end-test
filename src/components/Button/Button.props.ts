import type { MouseEventHandler, ReactElement } from 'react'

export interface ButtonProps {
  label: string
  onClick: MouseEventHandler
  id?: string
  dataTestid?: string
  primary?: boolean
  disabled?: boolean
  leftIcon?: ReactElement
  rightIcon?: ReactElement
}
