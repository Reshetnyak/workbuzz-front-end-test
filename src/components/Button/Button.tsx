import React from 'react'
import { Typography } from '@mui/material'
import type { ButtonProps } from './Button.props'
import { ButtonContent, ButtonWrapper } from './Button.s'

const Button: React.FC<ButtonProps> = ({
  label,
  onClick,
  id,
  dataTestid,
  leftIcon,
  rightIcon,
  primary = true,
  disabled = false
}: ButtonProps) => {
  return (
    <ButtonWrapper
      id={id}
      primary={primary}
      disabled={disabled}
      data-testid={dataTestid}
      onClick={onClick}>
      <ButtonContent primary={primary}>
        {leftIcon}
        <Typography
          variant="body1"
          component="span"
          ml={(leftIcon != null) ? '0.5rem' : ''}
          mr={(rightIcon != null) ? '0.5rem' : ''}
        >{label}</Typography>
        {rightIcon}
      </ButtonContent>
    </ButtonWrapper>
  )
}

export default Button
