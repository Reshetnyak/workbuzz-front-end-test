import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, useTheme } from '@mui/material'

import ButtonComponent from './Button'
import plus from '../../assets/plus.svg'
import leftArrow from '../../assets/leftArrow.svg'
import rightArrow from '../../assets/rightArrow.svg'
import type { ButtonProps } from './Button.props'

export default {
  title: 'Buttons/Button',
  component: ButtonComponent,
  argTypes: {
    label: {
      type: 'string',
      description: 'Button text'
    },
    onClick: {
      type: 'function',
      description: 'Click handler'
    },
    primary: {
      type: 'boolean',
      description: 'If `true` the button has primary styles',
      defaultValue: true
    },
    disabled: {
      type: 'boolean',
      description: 'If `true` the button is disabled',
      default: false
    },
    leftIcon: {
      type: 'function',
      description: 'Icon from the left of the button text'
    },
    rightIcon: {
      type: 'function',
      description: 'Icon from the right of the button text'
    }
  },
  args: {
    label: 'Button Component',
    disabled: false
  },
  parameters: {
    controls: { expanded: true }
  }
} as ComponentMeta<typeof ButtonComponent>

const Template: ComponentStory<typeof ButtonComponent> = (args) => <ButtonComponent {...args}/>

export const Button = Template.bind({})

const noop = (): void => {}
const primaryProps: ButtonProps = {
  label: 'Primary',
  leftIcon: <img src={plus} width={14} />,
  onClick: noop
}
const secondaryProps = {
  label: 'Secondary',
  primary: false,
  leftIcon: <img src={leftArrow} width={17} />,
  rightIcon: <img src={rightArrow} width={17} />,
  onClick: noop
}

const rows = [
  {
    state: 'Default',
    primary: <ButtonComponent {...primaryProps}/>,
    secondary: <ButtonComponent {...secondaryProps}/>
  },
  {
    state: 'Hover',
    primary: <ButtonComponent {...primaryProps} id="primary-hover"/>,
    secondary: <ButtonComponent {...secondaryProps} id="secondary-hover"/>
  },
  {
    state: 'Focus',
    primary: <ButtonComponent {...primaryProps} id="primary-focus"/>,
    secondary: <ButtonComponent {...secondaryProps} id="secondary-focus"/>
  },
  {
    state: 'Active',
    primary: <ButtonComponent {...primaryProps} id="primary-active"/>,
    secondary: <ButtonComponent {...secondaryProps} id="secondary-active"/>
  },
  {
    state: 'Disabled',
    primary: <ButtonComponent {...primaryProps} disabled/>,
    secondary: <ButtonComponent {...secondaryProps} disabled/>
  }
]

export const All: ComponentStory<typeof ButtonComponent> = () => {
  const theme = useTheme()
  return <TableContainer component={Paper} sx={{ maxWidth: 650 }}>
      <Table sx={{ backgroundColor: theme.palette.background.default }}>
        <TableHead>
          <TableRow>
            <TableCell align="center"><Typography>State \ Button Type</Typography></TableCell>
            <TableCell align="center"><Typography>Primary</Typography></TableCell>
            <TableCell align="center"><Typography>Secondary</Typography></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.state}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                <Typography>{row.state}</Typography>
              </TableCell>
              <TableCell align="center">{row.primary}</TableCell>
              <TableCell align="center">{row.secondary}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
}

All.parameters = {
  pseudo: {
    hover: ['#primary-hover', '#secondary-hover'],
    focus: ['#primary-focus', '#secondary-focus'],
    active: ['#primary-active', '#secondary-active']
  }
}
