import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Button from './Button'

it('should render primary Button component', () => {
  const label = 'Primary'
  const { getByText } = render(<Button label={label} onClick={() => {}}/>)
  expect(getByText(label)).toBeInTheDocument()
})

it('should render secondary Button component', () => {
  const label = 'Secondary'
  const { getByText } = render(<Button label={label} primary={false} onClick={() => {}}/>)
  expect(getByText(label)).toBeInTheDocument()
})
