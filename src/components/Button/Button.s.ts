import { styled } from '@mui/material'
import type { ButtonProps } from './Button.props'

const triangleSize = 16

export const ButtonWrapper = styled('button')<Omit<ButtonProps, 'label'>>(({ theme, primary = true, disabled = false }) => {
  const backgroundColor = primary ? theme.palette.primary.main : theme.palette.neutral30
  const hoverBgColor = primary ? theme.palette.primary20 : theme.palette.neutral10

  return {
    color: theme.palette.white,
    backgroundColor: 'transparent',
    cursor: disabled ? 'default' : 'pointer',
    border: 'none',
    padding: 0,
    ':hover': {
      '::before': {
        borderColor: hoverBgColor
      },
      '::after': {
        borderColor: hoverBgColor
      },
      '& > p': {
        backgroundColor: hoverBgColor,
        '::before': {
          borderColor: `${hoverBgColor} transparent`
        },
        '::after': {
          borderColor: `transparent ${hoverBgColor}`
        }
      }
    },
    ':hover:disabled': {
      '::before': {
        borderColor: backgroundColor
      },
      '::after': {
        borderColor: backgroundColor
      },
      '& > p': {
        backgroundColor,
        '::before': {
          borderColor: `${backgroundColor} transparent`
        },
        '::after': {
          borderColor: `transparent ${backgroundColor}`
        }
      }
    },
    ':focus': {
      outline: `2px solid ${theme.palette.white}`
    },
    ':active': {
      outline: `2px solid ${theme.palette.white}`,
      '::before': {
        borderColor: hoverBgColor
      },
      '::after': {
        borderColor: hoverBgColor
      },
      '& > p': {
        backgroundColor: hoverBgColor,
        '::before': {
          borderColor: `${hoverBgColor} transparent`
        },
        '::after': {
          borderColor: `transparent ${hoverBgColor}`
        }
      }
    },
    ':disabled': {
      opacity: 0.5,
      outline: 'none',
      backgroundColor: 'transparent'
    },
    '::before, ::after': {
      display: 'block',
      content: '""',
      position: 'relative',
      borderStyle: 'solid',
      borderWidth: `0 0 ${triangleSize}px ${triangleSize}px`,
      borderColor: backgroundColor,
      height: `${triangleSize}px`
    },
    '::before': {
      top: 0,
      marginLeft: `${triangleSize}px`
    },
    '::after': {
      bottom: 0,
      marginRight: `${triangleSize}px`
    },
    '& > p': {
      backgroundColor
    }
  }
})

export const ButtonContent = styled('p')<{ primary: boolean }>(({ theme, primary }) => {
  const backgroundColor = primary ? theme.palette.primary.main : theme.palette.neutral30
  return {
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
    margin: 0,
    backgroundColor,
    padding: `0 ${triangleSize}px 0 ${triangleSize}px`,
    '::before, ::after': {
      display: 'block',
      content: '""',
      position: 'absolute',
      borderStyle: 'solid',
      borderWidth: `0 0 ${triangleSize}px ${triangleSize}px`,
      height: `${triangleSize}px`
    },
    '::before': {
      top: `-${triangleSize}px`,
      left: 0,
      borderColor: `${backgroundColor} transparent`
    },
    '::after': {
      bottom: `-${triangleSize}px`,
      right: 0,
      borderColor: `transparent ${backgroundColor}`
    }
  }
})
