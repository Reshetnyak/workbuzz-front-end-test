import React from 'react'
import { Box, Grid, Stack, Typography } from '@mui/material'
import DOMPurify from 'dompurify'

import type { CardProps } from './Card.props'
import { AddButtonContainer, StyledAddButton, Image } from './Card.s'

const Card: React.FC<CardProps> = ({
  title,
  published,
  writer,
  description,
  imgUrl,
  onAdd
}) => {
  // TODO: Clarify approach for DOM purification
  const htmlRegex = /<[a-z][\s\S]*>/i
  const hasDescriptionHTML = htmlRegex.test(description)
  return (
    // TODO: revrite it to use CSS Grid instead of flexes and components
    <Grid container spacing={3} style={{ justifyContent: 'center' }}>
      <Grid item md={6} sm={12} style={{ flex: '1 1 auto', display: 'flex', position: 'relative', justifyContent: 'center' }}>
        <Box style={{ flex: '0 1 auto' }}>
          <Image src={imgUrl} alt={title} />
          <AddButtonContainer>
            <StyledAddButton onClick={onAdd}></StyledAddButton>
          </AddButtonContainer>
        </Box>
      </Grid>
      {/** TODO: discuss a way to display cases with long and the absense of a description */}
      <Grid item md={6} sm={12} style={{ flex: '1 1 auto', display: 'flex', maxHeight: '462px', overflow: 'auto' }}>
        <Stack spacing={{ xs: 1, md: 1, lg: 3 }} direction={'column'} style={{ flex: '1 1 auto' }}>
          <Typography variant={'h3'}>{title}</Typography>
          <div>
            <Typography variant={'subtitle1'}>Published:</Typography>
            <Typography variant={'body1'}>{published}</Typography>
          </div>
          <div>
            <Typography variant={'subtitle1'}>Writer:</Typography>
            <Typography variant={'body1'}>{writer}</Typography>
          </div>
          <Typography variant={'body2'}>
            {hasDescriptionHTML
              ? <span dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(description) }}></span>
              : description
            }
          </Typography>
        </Stack>
      </Grid>
    </Grid>
  )
}

export default Card
