export interface CardProps {
  title: string
  published: string
  writer: string
  description: string
  imgUrl: string
  onAdd: (id: number) => void
}
