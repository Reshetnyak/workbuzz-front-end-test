import { styled } from '@mui/material'
import AddButton from '../AddButton/AddButton'

export const AddButtonContainer = styled('div')(() => ({
  position: 'absolute',
  bottom: '1rem',
  left: '50%',
  transform: 'translate(-50%)'
}))

export const StyledAddButton = styled(AddButton)(() => ({
  margin: '0 auto'
}))

export const Image = styled('img')(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    width: 360
  },
  [theme.breakpoints.up('lg')]: {
    width: 280
  }
}))
