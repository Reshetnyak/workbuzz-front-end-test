import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'
import { Container, Grid } from '@mui/material'

import CardComponent from './Card'

export default {
  title: 'Components/Card',
  component: CardComponent,
  argTypes: {
    title: {
      type: 'string',
      description: 'Comic\'s title'
    },
    published: {
      type: 'string',
      description: 'Comic\'s published date'
    },
    writer: {
      type: 'string',
      description: 'Comic\'s writer/s'
    },
    description: {
      type: 'string',
      description: 'Comic\'s description'
    },
    imgUrl: {
      type: 'string',
      description: 'Comic\'s cover image url'
    },
    onAdd: {
      type: 'function',
      description: 'Add button click handler'
    }
  },
  args: {
    title: 'Card Component',
    onAdd: e => { console.log(e) }
  },
  parameters: {
    controls: { expanded: true }
  }
} as ComponentMeta<typeof CardComponent>

const Template: ComponentStory<typeof CardComponent> = (args) => <CardComponent {...args}/>

export const Card: React.FC = () => <Container style={{ maxWidth: '1200px' }}><Template
  title={'Avengers: The Initiative (2007) #19'}
  published={'December 17, 2008'}
  writer={'Christos Gage, Dan Slott'}
  description={'Join 3-D MAN, CLOUD 9, KOMODO, HARDBALL, and heroes around America in the battle that will decide the fate of the planet and the future of the Initiative program. Will the Kill Krew Army win the day?'}
  imgUrl={'https://cdn.marvel.com/u/prod/marvel/i/mg/d/03/58dd080719806/clean.jpg'}
  onAdd={e => { console.log('Add Avengers: The Initiative (2007) #19') }}/>
</Container>

export const CardsInsideLayout: React.FC = () =>
  <Container maxWidth='lg'>
    <Grid container spacing={3}>
      <Grid item lg={6} md={12}>
        <CardComponent
          title={'Avengers: The Initiative (2007) #19'}
          published={'December 17, 2008'}
          writer={'Christos Gage, Dan Slott'}
          description={'Join 3-D MAN, CLOUD 9, KOMODO, HARDBALL, and heroes around America in the battle that will decide the fate of the planet and the future of the Initiative program. Will the Kill Krew Army win the day?'}
          imgUrl={'https://cdn.marvel.com/u/prod/marvel/i/mg/d/03/58dd080719806/clean.jpg'}
          onAdd={e => { console.log('Add Avengers: The Initiative (2007) #19') }}
        />
      </Grid>
      <Grid item lg={6} md={12}>
        <CardComponent
          title={'Avengers: The Initiative (2007) #18'}
          published={'October 29, 2008'}
          writer={'Christos Gage, Dan Slott'}
          description={'Now that the Kill Krew knows Skrullowjacket\'s master plan, can they stop the true mission of the Fifty State Initiative? Plus, Thor Girl vs. Ultra Girl! One is more than she appears to be and the other\'s a Skrull!'}
          imgUrl={'https://cdn.marvel.com/u/prod/marvel/i/mg/2/20/58dd09cca3bcd/detail.jpg'}
          onAdd={e => { console.log('Add Avengers: The Initiative (2007) #18') }}
        />
      </Grid>
    </Grid>
  </Container>
