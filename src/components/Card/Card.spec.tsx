import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Card from './Card'

it('should render Card component', () => {
  const description = 'Join 3-D MAN, CLOUD 9, KOMODO, HARDBALL, and heroes around America in the battle that will decide the fate of the planet and the future of the Initiative program. Will the Kill Krew Army win the day?'
  const { getByText } = render(<Card
    title={'Avengers: The Initiative (2007) #19'}
    published={'December 17, 2008'}
    writer={'Christos Gage, Dan Slott'}
    description={description}
    imgUrl={'https://cdn.marvel.com/u/prod/marvel/i/mg/d/03/58dd080719806/clean.jpg'}
    onAdd={e => { console.log('Add Avengers: The Initiative (2007) #19') }}/>)
  expect(getByText('Avengers: The Initiative (2007) #19')).toBeInTheDocument()
  expect(getByText('December 17, 2008')).toBeInTheDocument()
  expect(getByText('Christos Gage, Dan Slott')).toBeInTheDocument()
  expect(getByText(description)).toBeInTheDocument()
  const displayedImage = document.querySelector('img') as HTMLImageElement
  expect(displayedImage.src).toContain('58dd080719806/clean')
})
