import React from 'react'

import { Label, Total } from './ReadingListLink.s'
import type { ReadingListLinkProps } from './ReadingListLink.props'
import Link from '../Link/Link'

const ReadingListLink: React.FC<ReadingListLinkProps> = ({
  onClick,
  label = 'Reading List',
  total = 0
}) => (
  <Link onClick={onClick}>
    <Label variant='body1'>{label}</Label>
    <Total variant='body1'>{`(${total})`}</Total>
  </Link>
)

export default ReadingListLink
