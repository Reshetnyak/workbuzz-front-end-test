
import { styled, Typography } from '@mui/material'

export const Label = styled(Typography)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    display: 'none'
  },
  [theme.breakpoints.up('lg')]: {
    display: 'inline-block'
  }
}))

export const Total = styled(Typography)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    marginLeft: 0
  },
  [theme.breakpoints.up('lg')]: {
    marginLeft: '0.5rem'
  }
}))
