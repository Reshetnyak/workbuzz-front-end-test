import type { MouseEventHandler } from 'react'

export interface ReadingListLinkProps {
  total: number
  onClick: MouseEventHandler
  label?: string
}
