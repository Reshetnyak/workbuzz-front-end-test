import React from 'react'
import type { ComponentStory, ComponentMeta } from '@storybook/react'

import ReadingListLinkComponent from './ReadingListLink'
import { Box } from '@mui/material'

export default {
  title: 'Components/Reading List Link',
  component: ReadingListLinkComponent,
  argTypes: {
    total: {
      type: 'number',
      description: 'Number of items added to the Reading List',
      required: true
    },
    onClick: {
      type: 'function',
      description: 'Click handler',
      required: true
    },
    label: {
      type: 'string',
      description: 'Link text',
      defaultValue: 'Reading List'
    }
  },
  parameters: {
    controls: { expanded: true }
  }
} as ComponentMeta<typeof ReadingListLinkComponent>

const Template: ComponentStory<typeof ReadingListLinkComponent> = (args) => <Box sx={{ display: 'flex' }}>
  <ReadingListLinkComponent {...args}/>
</Box>

export const ReadingListLink = Template.bind({})

ReadingListLink.args = {
  total: 10
}
