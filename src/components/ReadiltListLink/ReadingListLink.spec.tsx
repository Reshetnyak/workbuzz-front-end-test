import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import ReadingListLink from './ReadingListLink'

it('should render Reading List Link component', () => {
  const { getByText } = render(<ReadingListLink total={20} onClick={e => { e.preventDefault() }}/>)
  expect(getByText('(20)')).toBeInTheDocument()
  expect(getByText('Reading List')).toBeInTheDocument()
})
