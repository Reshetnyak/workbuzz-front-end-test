import { Md5 } from 'ts-md5'

export interface GetUrlParams {
  /** url without search params */
  base: string
  /** params to be added to the search query */
  searchParams: Record<string, string>
  /** request timestamp in ms */
  ts?: number
}

export const getUrl = ({
  base,
  searchParams,
  ts = Date.now()
}: GetUrlParams): string => {
  const url = new URL(base)
  const params = new URLSearchParams()
  const md5 = new Md5()

  Object.entries(searchParams).forEach(([key, value]) => {
    params.append(key, value)
  })

  const hash = md5.appendStr(ts.toString())
    .appendStr(import.meta.env.VITE_MARVEl_PRIVATE_KEY)
    .appendStr(import.meta.env.VITE_MARVEL_PUBLIC_KEY)
    .end(false) as string // because md5 doesn't have overloads and returned type doesn't depend on provided argument

  params.append('ts', ts.toString())
  params.append('apikey', import.meta.env.VITE_MARVEL_PUBLIC_KEY)
  params.append('hash', hash)

  url.search = params.toString()

  return url.toString()
}
