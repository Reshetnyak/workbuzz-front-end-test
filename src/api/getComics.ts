import { useEffect, useState } from 'react'
import { CHARACTER_API_BASE, COMICS_API_PATH, DEFAULT_CHARACTERS_ID, DEFAULT_COMICS_LIMIT } from './consts'
import { getUrl } from './getUrl'

export interface ComicImage {
  extension: string
  path: string
}

export interface ComicCreator {
  name: string
  role: 'writer' | 'editor' | 'letterer' | 'penciller' | 'colorist' | 'inker'
  resourceURI: string
}

export interface ComicCreators {
  items: ComicCreator[]
  [key: string]: unknown
}

export interface ComicDate {
  type: 'onsaleDate' | 'focDate' | 'unlimitedDate' | 'digitalPurchaseDate'
  date: string // '2008-10-29T00:00:00-0400'
}

export interface Comic {
  creators: ComicCreators
  dates: ComicDate[]
  description: string
  images: ComicImage[]
  title: string
  [key: string]: unknown
}

export interface UseComicsReturnType {
  comics: Comic[]
  isLoading: boolean
  error: Error | undefined
}

export const useComics = (offset: number, id = DEFAULT_CHARACTERS_ID, limit = DEFAULT_COMICS_LIMIT): UseComicsReturnType => {
  const [comics, setComics] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState<Error>()

  useEffect(() => {
    const comicsUrl = getUrl({
      base: [CHARACTER_API_BASE, id, COMICS_API_PATH].join('/'),
      searchParams: {
        limit: limit.toString(),
        offset: (offset * limit).toString()
      }
    })
    const fetchData = async (): Promise<void> => {
      try {
        setIsLoading(true)
        const response = await fetch(comicsUrl)
        const body = await response.json()
        setComics(body.data.results)
        setIsLoading(false)
      } catch (e) {
        setError(e as Error)
      }
    }
    fetchData().catch(setError)
  }, [id, offset])

  return {
    comics,
    isLoading,
    error
  }
}
