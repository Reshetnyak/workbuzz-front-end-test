import { useEffect, useState } from 'react'
import { getUrl } from './getUrl'
import { DEFAULT_CHARACTERS_LIMIT, CHARACTER_API_BASE } from './consts'

export interface Character {
  id: number
  name: string
  [key: string]: unknown
}

export interface UseCharactersReturnType {
  characters: Character[]
  isLoading: boolean
  error: Error | undefined
}

const charactersUrl = getUrl({
  base: CHARACTER_API_BASE,
  searchParams: { limit: DEFAULT_CHARACTERS_LIMIT.toString() }
})

export const useCharacters = (): UseCharactersReturnType => {
  const [characters, setCharacters] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState<Error>()

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      try {
        setIsLoading(true)
        const response = await fetch(charactersUrl)
        const body = await response.json()
        setCharacters(body.data.results)
        setIsLoading(false)
      } catch (e) {
        setError(e as Error)
      }
    }
    fetchData().catch(setError)
  }, [charactersUrl])

  return {
    characters,
    isLoading,
    error
  }
}
