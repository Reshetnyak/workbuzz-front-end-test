export const CHARACTER_API_BASE = 'https://gateway.marvel.com/v1/public/characters'
export const COMICS_API_PATH = 'comics'
export const DEFAULT_CHARACTERS_LIMIT = 5
export const DEFAULT_COMICS_LIMIT = 4
export const DEFAULT_CHARACTERS_ID = 1011334
