import React from 'react'
import { render, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
import FilterContainer from './FilterContainer'

const mockFetch = vi.fn()

beforeAll(() => {
  global.fetch = mockFetch
})

afterAll(() => {
  delete global.fetch
})

it('should render Filter Container', async () => {
  const mockItems = [
    { id: 1, name: 'First' },
    { id: 2, name: 'Second' },
    { id: 3, name: 'Third' }
  ]
  mockFetch.mockResolvedValueOnce({
    ok: true,
    json: vi.fn().mockResolvedValueOnce({
      data: {
        results: mockItems
      }
    })
  })

  const { getByText, findAllByText } = render(<FilterContainer
    onFilter={id => { console.log(id) }}
  />)

  const items = await findAllByText(/\w+/i)
  expect(items).toHaveLength(1)
  expect(items[0]).toHaveTextContent('Filter by character:')

  await waitFor(() => {
    expect(getByText('First')).toBeInTheDocument()
    expect(getByText('Second')).toBeInTheDocument()
    expect(getByText('Third')).toBeInTheDocument()
  })
})
