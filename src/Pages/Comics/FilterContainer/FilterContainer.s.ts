import { Box, styled } from '@mui/material'

export const FilterWrapper = styled(Box)(({ theme }) => ({
  marginTop: '1.5rem',
  [theme.breakpoints.down('lg')]: {
    marginBottom: '2.25rem'
  },
  [theme.breakpoints.up('lg')]: {
    marginBottom: '2.5rem'
  }
}))
