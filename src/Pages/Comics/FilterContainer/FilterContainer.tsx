import React, { memo } from 'react'

import type { FilterContainerProps } from './FilterContainer.props'
import { FilterWrapper } from './FilterContainer.s'
import Filter from '../../../components/Filter/Filter'
import { useCharacters } from '../../../api/getCharacters'

const FilterContainer: React.FC<FilterContainerProps> = ({
  onFilter
}) => {
  const {
    characters: rawCharacters,
    error: charactersError
  } = useCharacters()

  const characters = typeof charactersError !== 'undefined'
    // Just a silly way to handle an error. In real app I'd use agreed approach
    ? [{ id: 1, label: 'Unable to fetch characters. Please, try again later' }]
    : rawCharacters.map(({ id, name }) => ({ id, label: name }))

  return (
    <FilterWrapper>
      <Filter
        items={characters}
        onFilter={onFilter}
      />
    </FilterWrapper>
  )
}

export default memo(FilterContainer)
