export interface FilterContainerProps {
  onFilter: (id: number) => void
}
