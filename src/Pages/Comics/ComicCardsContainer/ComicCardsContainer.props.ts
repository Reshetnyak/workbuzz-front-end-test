export interface ComicCardsContainerProps {
  currentPage: number
  characterId: number
  onAdd: (id: number) => void
}
