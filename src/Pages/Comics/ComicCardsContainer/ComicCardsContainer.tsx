import React from 'react'
import { Grid } from '@mui/material'

import Card from '../../../components/Card/Card'
import { type ComicCreator, type ComicDate, type ComicImage, useComics } from '../../../api/getComics'
import { type ComicCardsContainerProps } from './ComicCardsContainer.props'

const ComicCardsContainer: React.FC<ComicCardsContainerProps> = ({
  currentPage,
  characterId,
  onAdd
}) => {
  const {
    comics
    // error: comicsError //TODO: Hadnle comics Eror
  } = useComics(currentPage, characterId)

  // TODO: Move comics transformation logic to either separate container component or standalone file
  const getPublishedDate = (dates: ComicDate[]): string => {
    let comicDate = dates.find(date => date.type === 'onsaleDate')
    // TODO: To clarify, what to do if date is missing
    if (typeof comicDate === 'undefined') {
      comicDate = { type: 'onsaleDate', date: '' }
    }

    return new Date(comicDate.date).toLocaleDateString('en-US', {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    })
  }

  const getWriters = (creators: ComicCreator[]): string => {
    const writers = creators.filter(creator => creator.role === 'writer')
    return writers.map(writer => writer.name).join(', ')
  }

  const getImageUrl = (images: ComicImage[]): string => {
    // TODO: Choose a placeholder image for no thumbnail cases
    return images.length > 0 ? [images[0].path, images[0].extension].join('.') : ''
  }

  const cards = comics.map(comic => ({
    title: comic.title,
    published: getPublishedDate(comic.dates), // 'December 17, 2008',
    writer: getWriters(comic.creators.items), // 'Christos Gage, Dan Slott',
    description: comic.description,
    imgUrl: getImageUrl(comic.images)
  }))

  return (
    <Grid container spacing={3} style={{ paddingLeft: '0.5rem' }}>
      {cards.map(card =>
        <Grid item lg={6} md={12} key={crypto.randomUUID()} sx={{ justifyContent: 'center' }}>
          <Card
            onAdd={onAdd}
            {...card}
          />
        </Grid>
      )}
    </Grid>
  )
}

export default ComicCardsContainer
