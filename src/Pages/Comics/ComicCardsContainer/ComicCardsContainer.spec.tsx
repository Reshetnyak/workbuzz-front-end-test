import React from 'react'
import { render, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
import ComicCardsContainer from './ComicCardsContainer'
import { type Comic } from '../../../api/getComics'

const mockFetch = vi.fn()
Object.defineProperty(global, 'crypto', {
  value: {
    randomUUID: () => Math.random()
  }
})

const mockItems: Comic[] = [
  {
    creators: {
      items: [{
        name: 'Writer 1',
        role: 'writer',
        resourceURI: ''
      }, {
        name: 'Editor 1',
        role: 'editor',
        resourceURI: ''
      }]
    },
    dates: [{
      type: 'onsaleDate',
      date: '2008-10-29T00:00:00-0400'
    }, {
      type: 'unlimitedDate',
      date: '2009-04-29T00:00:00-0400'
    }],
    description: 'Description 1',
    images: [{ extension: 'jpg', path: 'image-path-1' }],
    title: 'Title 1'
  },
  {
    creators: {
      items: [{
        name: 'Writer 2',
        role: 'writer',
        resourceURI: ''
      }, {
        name: 'Editor 2',
        role: 'editor',
        resourceURI: ''
      }]
    },
    dates: [{
      type: 'onsaleDate',
      date: '2007-07-27T00:00:00-0400'
    }, {
      type: 'unlimitedDate',
      date: '2009-04-29T00:00:00-0400'
    }],
    description: 'Description 2',
    images: [{ extension: 'jpg', path: 'image-path-2' }],
    title: 'Title 2'
  },
  {
    creators: {
      items: [{
        name: 'Writer 3',
        role: 'writer',
        resourceURI: ''
      }, {
        name: 'Editor 3',
        role: 'editor',
        resourceURI: ''
      },
      {
        name: 'Writer 4',
        role: 'writer',
        resourceURI: ''
      }]
    },
    dates: [{
      type: 'onsaleDate',
      date: '2009-09-29T00:00:00-0400'
    }, {
      type: 'unlimitedDate',
      date: '2009-04-29T00:00:00-0400'
    }],
    description: 'Description 3',
    images: [{ extension: 'jpg', path: 'image-path-3' }],
    title: 'Title 3'
  }
]
const mockData = {
  data: {
    results: mockItems
  }
}

beforeAll(() => {
  global.fetch = mockFetch
})

afterAll(() => {
  delete global.fetch
})

it('should render Comic Cards Container', async () => {
  mockFetch.mockResolvedValueOnce({
    ok: true,
    json: vi.fn().mockResolvedValueOnce(mockData)
  })

  const { getByText, getByAltText } = render(<ComicCardsContainer
    currentPage={0}
    characterId={0}
    onAdd={id => { console.log(id) }}
  />)

  await waitFor(() => {
    expect(getByText('Title 1')).toBeInTheDocument()
    expect(getByText('Writer 1')).toBeInTheDocument()
    expect(getByText('October 29, 2008')).toBeInTheDocument()
    expect(getByText('Description 1')).toBeInTheDocument()
    expect(getByText('Title 2')).toBeInTheDocument()
    expect(getByText('Writer 2')).toBeInTheDocument()
    expect(getByText('July 27, 2007')).toBeInTheDocument()
    expect(getByText('Description 2')).toBeInTheDocument()
    expect(getByAltText('Title 3')).toBeInTheDocument()
    expect(getByText('Writer 3, Writer 4')).toBeInTheDocument()
    expect(getByText('September 29, 2009')).toBeInTheDocument()
    expect(getByText('Description 3')).toBeInTheDocument()
  })
})
