import React, { useCallback, useState } from 'react'
import { Container } from '@mui/material'

import { Pagination } from './Comics.s'
import Navigation from '../../components/Navigation/Navigation'
import PrevButton from '../../components/PrevButton/PrevButton'
import NextButton from '../../components/NextButton/NextButton'
import { DEFAULT_CHARACTERS_ID } from '../../api/consts'
import FilterContainer from './FilterContainer/FilterContainer'
import ComicCardsContainer from './ComicCardsContainer/ComicCardsContainer'

const ComicsPage: React.FC = () => {
  const [characterId, setCharacterId] = useState(DEFAULT_CHARACTERS_ID)
  const [currentPage, setCurrentPage] = useState(0)

  console.count('Comics Page rendered')

  const onFilter = useCallback((id: number) => {
    setCharacterId(id)
    setCurrentPage(0)
  }, [])

  const onAdd = useCallback((id: number) => {
    console.log('add comic', id)
  }, [])

  return (
    <Container maxWidth='lg' sx={{ marginTop: '3rem' }}>
      <Navigation total={0} onListOpen={e => { e.preventDefault(); console.log('open list') }}/>
      {/* TODO: Check why Styles are not applied from Styled component. Too much time has been spent on trying to figure it out */}
      {/* <StyledFilter items={mockItems} onFilter={id => { console.log('filter', id) }}/> */}
      <FilterContainer
        onFilter={onFilter}
      />
      <ComicCardsContainer
        currentPage={currentPage}
        characterId={characterId}
        onAdd={onAdd}
      />
      <Pagination>
        <PrevButton
          disabled={currentPage === 0}
          onClick={e => { setCurrentPage(n => n === 0 ? 0 : n - 1) }}
        />
        <NextButton
          // TODO: improve UX by disabling next button
          onClick={e => { setCurrentPage(n => n + 1) }}
        />
      </Pagination>
    </Container>
  )
}

export default ComicsPage
