import { styled } from '@mui/material'

export const Pagination = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  gap: '0.5rem',
  [theme.breakpoints.down('lg')]: {
    marginTop: '3rem',
    marginBottom: '3rem'
  },
  [theme.breakpoints.up('lg')]: {
    marginTop: '3.75rem',
    marginBottom: '3rem'
  }
}))

/*
export const StyledFilter = styled(Filter)(({ theme }) => ({
  color: 'red',
  //  outline: '2px dotted purple !important',
  '& h6': {
    marginTop: '1.5rem',
    paddingTop: '3rem'
  },
  outline: '2px dotted white',
  [theme.breakpoints.down('lg')]: {
    marginBottom: '2.25rem'
  },
  [theme.breakpoints.up('lg')]: {
    marginBottom: '2.5rem'
  }
}))
// console.log(StyledFilter)
*/
